FROM golang:1.23-bookworm

WORKDIR /app

RUN mkdir /.cache \
  && chmod -R 777 /.cache \
  && go install github.com/air-verse/air@latest \
  && go install github.com/a-h/templ/cmd/templ@latest

COPY go.mod go.sum ./
RUN go mod download

RUN wget -O tailwind --progress=dot:giga https://github.com/tailwindlabs/tailwindcss/releases/download/v4.0.0-alpha.30/tailwindcss-linux-x64 \
  && chmod +x tailwind

CMD ["air", "-c", ".air.toml"]
