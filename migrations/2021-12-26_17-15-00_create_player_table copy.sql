CREATE TABLE IF NOT EXISTS `player` (
    `uuid` CHAR(22) NOT NULL PRIMARY KEY,
    `group_uuid` CHAR(22) NOT NULL,
    `name` VARCHAR(32) NOT NULL,
    `recipient_uuid` CHAR(22) NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `fk_group_uuid` FOREIGN KEY (`group_uuid`) REFERENCES `group`(`uuid`)
);
