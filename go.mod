module gitlab.com/JankTech/secret-santa

go 1.23

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/tursodatabase/libsql-client-go v0.0.0-20240902231107-85af5b9d094d
	gitlab.com/JankTech/uuid v0.0.0-20241021224125-25bce795b21f
	github.com/a-h/templ v0.2.778
)

require (
	github.com/antlr4-go/antlr/v4 v4.13.0 // indirect
	github.com/coder/websocket v1.8.12 // indirect
	github.com/google/uuid v1.6.0 // indirect
	golang.org/x/exp v0.0.0-20240325151524-a685a6edb6d8 // indirect
)
