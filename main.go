package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"log/slog"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/JankTech/secret-santa/database"
	"gitlab.com/JankTech/secret-santa/database/models"
	"gitlab.com/JankTech/secret-santa/static"
	"gitlab.com/JankTech/secret-santa/templates/web"
	"gitlab.com/JankTech/secret-santa/templates/web/components"
)

type player string

type playerList struct {
	Players []player `json:"players"`
}

type repository interface {
	SaveGroup(g models.Group) error
	SavePlayer(p models.Player) error
	GetPlayer(uuid string) (models.Player, error)
}

var webTemplateDirectories = []string{"templates/web", "templates/web/layouts"}

func main() {
	log.Println("Server starting")

	var db database.DB
	var err error
	dbType := strings.ToLower(os.Getenv("DB_TYPE"))
	switch dbType {
	case "mysql":
		db, err = database.NewMysql()
	case "turso":
		db, err = database.NewTurso(os.Getenv("TURSO_URL"))
	default:
		panic(fmt.Sprintf("unknown database type `%s`", dbType))
	}
	if err != nil {
		panic(err)
	}

	repo := database.NewRepository(db)

	fSys, err := fs.Sub(static.Public, "public")
	if err != nil {
		panic(err)
	}

	http.Handle("GET /static/", http.StripPrefix("/static/", http.FileServer(http.FS(fSys))))

	http.HandleFunc("GET /player-input", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		t := components.PlayerInput(false)
		w.WriteHeader(http.StatusOK)
		err := t.Render(ctx, w)
		if err != nil {
			slog.ErrorContext(ctx, "rendering player_input template", slog.Any("err", err))
			return
		}
	})

	http.HandleFunc("POST /", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		err := r.ParseForm()
		if err != nil {
			writeError(w, err, http.StatusInternalServerError)
			return
		}
		p := r.Form["players[]"]
		filtered := []player{}
		for _, str := range p {
			s := strings.TrimSpace(str)
			if s != "" {
				filtered = append(filtered, player(s))
			}
		}

		pl := playerList{Players: filtered}
		linkMap, err := generate(repo, pl)

		links := []components.Link{}
		for name, url := range linkMap {
			links = append(links, components.Link{Name: name, Url: url})
		}
		t := components.PlayerLinks(links)
		w.WriteHeader(http.StatusOK)
		err = t.Render(ctx, w)
		if err != nil {
			slog.ErrorContext(ctx, "rendering player_links template", slog.Any("err", err))
			return
		}
	})

	http.HandleFunc("GET /", renderGeneratePage())
	http.HandleFunc("GET /{id}", renderRecipientPage(repo))
	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
	}
	http.ListenAndServe(":"+port, nil)
}

func renderRecipientPage(repo repository) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		uuid := r.PathValue("id")

		santa, err := repo.GetPlayer(uuid)
		if errors.Is(err, database.ErrNotFound) {
			writeError(w, err, http.StatusNotFound)
			return
		}
		if err != nil {
			writeError(w, err, http.StatusInternalServerError)
			return
		}

		recipient, err := repo.GetPlayer(santa.RecipientUUID)
		if errors.Is(err, database.ErrNotFound) {
			writeError(w, err, http.StatusNotFound)
			return
		}
		if err != nil {
			writeError(w, err, http.StatusInternalServerError)
			return
		}

		t := web.RecipientPage(web.Recipient{
			SantaName:     santa.Name,
			RecipientName: recipient.Name,
		})
		w.WriteHeader(http.StatusOK)
		err = t.Render(ctx, w)
		if err != nil {
			slog.ErrorContext(ctx, "rendering editor template", slog.Any("err", err))
			return
		}
	}
}

func renderGeneratePage() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		t := web.Generate()
		w.WriteHeader(http.StatusOK)
		err := t.Render(ctx, w)
		if err != nil {
			slog.ErrorContext(ctx, "rendering editor template", slog.Any("err", err))
			return
		}
	}
}

func generate(repo repository, pl playerList) (map[string]string, error) {
	shuffledPlayerList := shufflePlayers(pl.Players)
	groupModel, shuffledPlayerModels := generateModels(shuffledPlayerList)

	mappedModels := mapSantas(shuffledPlayerModels)

	err := saveGroup(repo, groupModel, mappedModels)
	if err != nil {
		return nil, err
	}

	return generateLinkMap(mappedModels), nil
}

// generateLinkMap maps each player to a link where they can retreive their recipient's name
func generateLinkMap(players []models.Player) map[string]string {
	l := make(map[string]string)
	for _, p := range players {
		l[p.Name] = os.Getenv("BASE_URL") + "/" + p.RecipientUUID
	}

	return l
}

// generateModels generates new models for the group and players
func generateModels(players []player) (models.Group, []models.Player) {
	groupModel := models.NewGroup()

	playerModels := make([]models.Player, 0)
	for _, name := range players {
		p := models.NewPlayer(groupModel, string(name))
		playerModels = append(playerModels, p)
	}

	return groupModel, playerModels
}

// shufflePlayers shuffles a slice of player
func shufflePlayers(pl []player) []player {
	rand.New(rand.NewSource(time.Now().UnixNano())).
		Shuffle(len(pl), func(i, j int) {
			pl[i], pl[j] = pl[j], pl[i]
		})

	return pl
}

// Map each player to the next player in the slice
func mapSantas(playerModels []models.Player) []models.Player {
	for idx, p := range playerModels {
		if idx == 0 {
			playerModels[len(playerModels)-1].SetRecipient(p)
		} else {
			playerModels[idx-1].SetRecipient(p)
		}
	}

	return playerModels
}

func saveGroup(repo repository, group models.Group, players []models.Player) error {
	err := repo.SaveGroup(group)
	if err != nil {
		return err
	}

	for _, p := range players {
		err = repo.SavePlayer(p)
		if err != nil {
			return err
		}
	}

	return nil
}

func writeError(w http.ResponseWriter, err error, httpStatusCode int) {
	log.Println("Error (" + fmt.Sprint(httpStatusCode) + "):" + err.Error())
	w.WriteHeader(httpStatusCode)
}
