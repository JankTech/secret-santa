package models

import (
	"time"

	"gitlab.com/JankTech/uuid"
)

type Group struct {
	CreatedAt time.Time
	UpdatedAt time.Time

	UUID string
}

func NewGroup() Group {
	return Group{
		UUID:      uuid.New().ToStringBase64URL(),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}
