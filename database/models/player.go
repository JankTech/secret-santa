package models

import (
	"time"

	"gitlab.com/JankTech/uuid"
)

type Players []Player

type Player struct {
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`

	UUID          string `json:"uuid"`
	GroupUUID     string `json:"groupUuid"`
	Name          string `json:"name"`
	RecipientUUID string `json:"recipientUuid"`
}

func NewPlayer(group Group, name string) Player {
	return Player{
		UUID:      uuid.New().ToStringBase64URL(),
		GroupUUID: group.UUID,
		Name:      name,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}

func (p *Player) SetRecipient(player Player) {
	p.RecipientUUID = player.UUID
}
