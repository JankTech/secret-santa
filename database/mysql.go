package database

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type mysql struct {
	db *sql.DB
}

func NewMysql() (DB, error) {
	db, err := sql.Open("mysql", generateMySQLDSN())
	if err != nil {
		return nil, err
	}

	return mysql{
		db: db,
	}, nil
}

func (svr mysql) GetConnection() *sql.DB {
	return svr.db
}

func generateMySQLDSN() string {
	validateEnvVarSet("MARIADB_USER")
	validateEnvVarSet("MARIADB_PASSWORD")
	validateEnvVarSet("MARIADB_CONTAINER_NAME")
	validateEnvVarSet("MARIADB_DATABASE")

	return os.Getenv("MARIADB_USER") + ":" + os.Getenv("MARIADB_PASSWORD") +
		"@tcp(" + os.Getenv("MARIADB_CONTAINER_NAME") + ")/" + os.Getenv("MARIADB_DATABASE") +
		"?charset=utf8mb4"
}
