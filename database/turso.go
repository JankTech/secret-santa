package database

import (
	"database/sql"

	_ "github.com/tursodatabase/libsql-client-go/libsql"
)

type turso struct {
	db *sql.DB
}

func NewTurso(dsn string) (DB, error) {
	db, err := sql.Open("libsql", dsn)
	if err != nil {
		return nil, err
	}

	return turso{
		db: db,
	}, nil
}

func (svr turso) GetConnection() *sql.DB {
	return svr.db
}
