package database

import (
	"database/sql"
	"errors"
	"time"

	"gitlab.com/JankTech/secret-santa/database/models"
)

var ErrNotFound = errors.New("not found")

type repository struct {
	DB DB
}

type DB interface {
	GetConnection() *sql.DB
}

func NewRepository(db DB) repository {
	return repository{
		DB: db,
	}
}

func (r repository) SaveGroup(g models.Group) error {
	q := "INSERT INTO `group` (`uuid`, `created_at`, `updated_at`) VALUES (?, ?, ?);"
	stmt, err := r.DB.GetConnection().Prepare(q)
	if err != nil {
		return err
	}

	g.UpdatedAt = time.Now()
	res, err := stmt.Exec(g.UUID, g.CreatedAt.Format("2006-01-02 15:04:05"), g.UpdatedAt.Format("2006-01-02 15:04:05"))
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected != 1 {
		return errors.New("could not save group")
	}

	return nil
}

func (r repository) SavePlayer(p models.Player) error {
	s := "INSERT INTO `player` (`uuid`,`group_uuid`, `name`, `recipient_uuid`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?, ?);"
	stmt, err := r.DB.GetConnection().Prepare(s)
	if err != nil {
		return err
	}

	p.UpdatedAt = time.Now()
	res, err := stmt.Exec(
		p.UUID,
		p.GroupUUID,
		p.Name,
		p.RecipientUUID,
		p.CreatedAt.Format("2006-01-02 15:04:05"),
		p.UpdatedAt.Format("2006-01-02 15:04:05"),
	)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected != 1 {
		return errors.New("could not save player")
	}

	return nil
}

func (r repository) GetPlayer(uuid string) (models.Player, error) {
	q := "SELECT `uuid`, `group_uuid`, `name`, `recipient_uuid`, `created_at`, `updated_at` FROM `player` WHERE `uuid` = ?;"

	p := models.Player{}
	row := r.DB.GetConnection().QueryRow(q, uuid)
	err := row.Scan(
		&p.UUID,
		&p.GroupUUID,
		&p.Name,
		&p.RecipientUUID,
		&p.CreatedAt,
		&p.UpdatedAt,
	)
	if errors.Is(err, sql.ErrNoRows) {
		return p, ErrNotFound
	}
	if err != nil {
		return p, err
	}

	return p, nil
}

// TODO: Save multiple players in one statement to save on requests
