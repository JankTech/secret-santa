package database

import (
	"os"
)

func validateEnvVarSet(s string) {
	if os.Getenv(s) == "" {
		panic("Environment variable `" + s + "` not set and is required.")
	}
}
