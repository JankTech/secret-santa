package components

import "fmt"

type Link struct {
	Name string
	Url  string
}

script copyLink(target string) {
  // Select the text from the textarea
  const textToCopy = document.getElementById(target);
  textToCopy.disabled = false;
  textToCopy.select();
  textToCopy.setSelectionRange(0, 99999); // For mobile devices
  document.execCommand("copy");
  textToCopy.disabled = true;
  textToCopy.parentElement.parentElement.querySelector("span.default-message").classList.add("hidden");
  textToCopy.parentElement.parentElement.querySelector("span.success-message").classList.remove("hidden");
  textToCopy.parentElement.parentElement.querySelector("span.success-message").classList.add("inline-flex");
  async function wait() {
    setTimeout(() => {
      textToCopy.parentElement.parentElement.querySelector("span.default-message").classList.remove("hidden");
      textToCopy.parentElement.parentElement.querySelector("span.success-message").classList.add("hidden");
      textToCopy.parentElement.parentElement.querySelector("span.success-message").classList.remove("inline-flex");
    }, 2000);
  }
  wait();
  window.getSelection().removeAllRanges()
}

script exportAsCSV(links []Link) {
  let csvData = "";
  for (let i = 0; i < links.length; i++) {
    csvData += links[i].Name + "," + links[i].Url + "\n";
  }
  let hiddenElement = document.createElement('a');
  hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvData);
  hiddenElement.target = '_blank';
  hiddenElement.download = "secret-santa.csv";
  hiddenElement.click();
}

templ PlayerLinks(links []Link) {
	<div id="links" class="max-w-sm mx-auto pt-10">
		<button
			type="button"
			onclick={ exportAsCSV(links) }
			class="py-2.5 px-5 me-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
		>Export as CSV</button>
		for i, l:= range links {
			<div class="ml-1 mt-6 mb-2 flex justify-between items-center">
				<label for={ fmt.Sprintf("santa-link-%d", i) } class="text-sm font-medium text-gray-900 dark:text-white">
					{ l.Name }:
				</label>
			</div>
			<div class="grid grid-cols-8 w-full max-w-[23rem] mb-2">
				<span class="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-2.5 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-s-lg dark:bg-gray-600 dark:text-white dark:border-gray-600">
					URL
				</span>
				<div class="relative w-full col-span-6">
					<input
						id={ fmt.Sprintf("santa-link-%d", i) }
						type="text"
						class="cursor-text bg-gray-50 border border-e-0 border-gray-300 text-gray-500 dark:text-gray-400 text-sm border-s-0 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
						value={ l.Url }
						disabled
						readonly
					/>
				</div>
				<button
					type="button"
					class="cursor-pointer flex-shrink-0 z-10 inline-flex items-center py-3 px-4 text-sm font-medium text-center text-white bg-blue-700 rounded-e-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 border border-blue-700 dark:border-blue-600 hover:border-blue-800 dark:hover:border-blue-700"
					onclick={ copyLink(fmt.Sprintf("santa-link-%d", i)) }
				>
					<span class="default-message" title="Copy">
						<svg class="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 18 20">
							<path d="M16 1h-3.278A1.992 1.992 0 0 0 11 0H7a1.993 1.993 0 0 0-1.722 1H2a2 2 0 0 0-2 2v15a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2Zm-3 14H5a1 1 0 0 1 0-2h8a1 1 0 0 1 0 2Zm0-4H5a1 1 0 0 1 0-2h8a1 1 0 1 1 0 2Zm0-5H5a1 1 0 0 1 0-2h2V2h4v2h2a1 1 0 1 1 0 2Z"></path>
						</svg>
					</span>
					<span class="success-message hidden items-center" title="Copied!">
						<svg class="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 12">
							<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5.917 5.724 10.5 15 1.5"></path>
						</svg>
					</span>
				</button>
			</div>
		}
	</div>
}
