# Secret Santa Generator API - written in GO

## To Run
1. Install docker and docker-compose
2. Copy .env.example to .env, change password fields, change BASE_URL field to be the base url that your api will be hosted at.
3. Run `docker-compose up -d`. This may need sudo priviledges depending on your setup

## Usage

### To generate
1. Send a POST request to the root path of your api domain with the body containing a json object with a single players array of player names. e.g.
```
{
    "players": [
        "luke",
        "karl",
        "tim",
        "chris"
    ]
}
```
2. The response will provide links that each player can visit to reveal their recipient.


### To lookup a player's recipient
1. Visit the link generated for the player in step 2 of the generate process above. The response should contain a json object of the recipient as a string
```
{
    "recipient":"tim"
}
```